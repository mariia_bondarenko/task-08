package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.initContainers();
		// sort (case 1)
		domController.sortXML();
		// save
		String outputXmlFile = "output.dom.xml";
		XMLUtils.saveXML(outputXmlFile,domController.getContainer());
		XMLUtils.validateXML(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.initContainers();
		// sort  (case 2)
		saxController.sortXML();
		// save
		outputXmlFile = "output.sax.xml";
		XMLUtils.saveXML(outputXmlFile,saxController.getContainer());
		XMLUtils.validateXML(outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.initContainer();
		// sort  (case 3)
		staxController.sortXML();
		// save
		outputXmlFile = "output.stax.xml";
		XMLUtils.saveXML(outputXmlFile,staxController.getContainer());
		XMLUtils.validateXML(outputXmlFile);
	}

}
