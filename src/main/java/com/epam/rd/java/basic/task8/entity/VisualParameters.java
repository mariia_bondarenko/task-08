package com.epam.rd.java.basic.task8.entity;

/*
<tn:stemColour>зеленая</tn:stemColour>
			<tn:leafColour>зеленая</tn:leafColour>
			<tn:aveLenFlower measure = "cm">10</tn:aveLenFlower>
 */
public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private short aveLenFlower;
    private String aveLenFlowerMeasure;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(short aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public String getAveLenFlowerMeasure() {
        return aveLenFlowerMeasure;
    }

    public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                ", aveLenFlowerMeasure='" + aveLenFlowerMeasure + '\'' +
                '}';
    }
}
