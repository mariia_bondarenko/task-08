package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingParameters;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Comparator;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;
	private Flowers flowers;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingParameters growingParameters;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getContainer(){
		return flowers;
	}

	private class SAXHandler extends DefaultHandler {
		String currentName;

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) {
			currentName = localName;

			if ("flowers".equals(localName)) {
				flowers = new Flowers();
			}

			if ("flower".equals(localName)) {
				flower = new Flower();
			}

			if ("visualParameters".equals(localName)) {
				visualParameters = new VisualParameters();
				flower.setVisualParameters(visualParameters);
			}

			if ("growingTips".equals(localName)) {
				growingParameters = new GrowingParameters();
				flower.setGrowingParameters(growingParameters);
			}

			if ("aveLenFlower".equals(localName)){
				visualParameters.setAveLenFlowerMeasure(attributes.getValue("measure"));
			}

			if ("tempreture".equals(localName)){
				growingParameters.setTemperatureMeasure(attributes.getValue("measure"));
			}

			if ("watering".equals(localName)){
				growingParameters.setWateringMeasure(attributes.getValue("measure"));
			}

			if ("lighting".equals(localName)){
				growingParameters.setLightRequiring(attributes.getValue("lightRequiring"));
			}

		}

		@Override
		public void characters(char[] ch, int start, int length) {
			String content =
					new String(ch, start, length).trim();

			if (content.isEmpty()) {
				return;
			}

			if ("flower".equals(currentName)) {
				flowers.getFlowers().add(flower);
			}
			if ("name".equals(currentName)) {
				flower.setName(content);
			}
			if ("soil".equals(currentName)) {
				flower.setSoil(content);
			}
			if ("origin".equals(currentName)) {
				flower.setOrigin(content);
			}
			if ("multiplying".equals(currentName)) {
				flower.setMultiplying(content);
			}
			if ("stemColour".equals(currentName)) {
				visualParameters.setStemColour(content);
			}
			if ("leafColour".equals(currentName)) {
				visualParameters.setLeafColour(content);
			}
			if ("aveLenFlower".equals(currentName)){
				visualParameters.setAveLenFlower(Short.parseShort(content));
			}
			if ("tempreture".equals(currentName)){
				growingParameters.setTempreture(Short.parseShort(content));
			}
			if ("watering".equals(currentName)){
				growingParameters.setWatering(Short.parseShort(content));
			}

		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			if ("flower".equals(localName)) {
				flowers.getFlowers().add(flower);
			}
		}
	}

	public void initContainers(){
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);

		try {
			factory.setFeature("http://xml.org/sax/features/validation", true);
			factory.setFeature("http://apache.org/xml/features/validation/schema", true);

			SAXParser parser = factory.newSAXParser();
			parser.parse(xmlFileName, new SAXHandler());

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	public void sortXML(){
		flowers.getFlowers().sort(Comparator.comparingInt(o -> o.getGrowingParameters().getWatering()));
	}

}