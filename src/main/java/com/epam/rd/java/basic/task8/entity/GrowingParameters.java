package com.epam.rd.java.basic.task8.entity;

/*<tn:tempreture measure = "celcius">25</tn:tempreture>
			<tn:lighting lightRequiring = "yes"/>
			<tn:watering measure = "mlPerWeek">110</tn:watering>*/
public class GrowingParameters {
    private short tempreture;
    private String lightRequiring;
    private short watering;

    private String temperatureMeasure;
    private String wateringMeasure;


    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(short tempreture) {
        this.tempreture = tempreture;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(short watering) {
        this.watering = watering;
    }

    public String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public void setTemperatureMeasure(String temperatureMeasure) {
        this.temperatureMeasure = temperatureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    @Override
    public String toString() {
        return "GrowingParameters{" +
                "tempreture=" + tempreture +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", watering=" + watering +
                ", temperatureMeasure='" + temperatureMeasure + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                '}';
    }
}
