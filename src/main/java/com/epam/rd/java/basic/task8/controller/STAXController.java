package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingParameters;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private Flowers flowers;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingParameters growingParameters;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getContainer(){
		return flowers;
	}

	public void initContainer(){
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = null;
		flowers = new Flowers();
		try {
			reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case "flower":
							flower = new Flower();
							break;
						case "name":
							nextEvent = reader.nextEvent();
							flower.setName(nextEvent.asCharacters().getData());
							break;
						case "soil":
							nextEvent = reader.nextEvent();
							flower.setSoil(nextEvent.asCharacters().getData());
							break;
						case "origin":
							nextEvent = reader.nextEvent();
							flower.setOrigin(nextEvent.asCharacters().getData());
							break;
						case "visualParameters":
							nextEvent = reader.nextEvent();
							visualParameters = new VisualParameters();
							break;
						case "stemColour":
							nextEvent = reader.nextEvent();
							visualParameters.setStemColour(nextEvent.asCharacters().getData());
							break;
						case "leafColour":
							nextEvent = reader.nextEvent();
							visualParameters.setLeafColour(nextEvent.asCharacters().getData());
							break;
						case "aveLenFlower":
							nextEvent = reader.nextEvent();
							visualParameters.setAveLenFlower(Short.parseShort(nextEvent.asCharacters().getData()));
							Attribute measure = startElement.getAttributeByName(new QName("measure"));
							visualParameters.setAveLenFlowerMeasure(measure.getValue());
							break;

						case "growingTips":
							nextEvent = reader.nextEvent();
							growingParameters = new GrowingParameters();
							break;
						case "tempreture":
							nextEvent = reader.nextEvent();
							growingParameters.setTempreture(Short.parseShort(nextEvent.asCharacters().getData()));
							Attribute measur = startElement.getAttributeByName(new QName("measure"));
							growingParameters.setTemperatureMeasure(measur.getValue());
							break;
						case "lighting":
							nextEvent = reader.nextEvent();
							Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
							growingParameters.setLightRequiring(lightRequiring.getValue());
							break;
						case "watering":
							nextEvent = reader.nextEvent();
							growingParameters.setWatering(Short.parseShort(nextEvent.asCharacters().getData()));
							Attribute measureTemp = startElement.getAttributeByName(new QName("measure"));
							growingParameters.setWateringMeasure(measureTemp.getValue());
							break;
						case "multiplying":
							nextEvent = reader.nextEvent();
							flower.setMultiplying(nextEvent.asCharacters().getData());
							break;
					}
				}
				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						flower.setVisualParameters(visualParameters);
						flower.setGrowingParameters(growingParameters);
						flowers.getFlowers().add(flower);
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void sortXML(){
		flowers.getFlowers().sort(Comparator.comparing(Flower::getOrigin));
	}
}