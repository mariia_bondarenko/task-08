package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingParameters;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Comparator;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getContainer(){
		return flowers;
	}

	public void initContainers(){
		DocumentBuilderFactory dbf =
				DocumentBuilderFactory.newInstance();

		dbf.setNamespaceAware(true);
		flowers = new Flowers();
		try {
			dbf.setFeature("http://xml.org/sax/features/validation", true);
			dbf.setFeature("http://apache.org/xml/features/validation/schema", true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(xmlFileName);
			Element root = doc.getDocumentElement();

			NodeList flowersNodes = root.getElementsByTagName("flower");
			for (int j = 0; j < flowersNodes.getLength(); j++){
				Flower f = getFlower(flowersNodes.item(j));
				flowers.getFlowers().add(f);
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	private static Flower getFlower(Node item) {
		Flower f = new Flower();
		if (item!= null) {
			NodeList flowerDetails = item.getChildNodes();
			f.setName(flowerDetails.item(1).getTextContent());
			f.setSoil(flowerDetails.item(3).getTextContent());
			f.setOrigin(flowerDetails.item(5).getTextContent());
			f.setVisualParameters(getVisualParameters(flowerDetails.item(7)));
			f.setGrowingParameters(getGrowingParameters(flowerDetails.item(9)));
			f.setMultiplying(flowerDetails.item(11).getTextContent());
		}
		return f;
	}
	private static VisualParameters getVisualParameters(Node item) {
		VisualParameters visualParameters = new VisualParameters();
		if (item!= null) {
			NodeList vpChildNodes = item.getChildNodes();
			visualParameters.setStemColour(vpChildNodes.item(1).getTextContent());
			visualParameters.setLeafColour(vpChildNodes.item(3).getTextContent());
			visualParameters.setAveLenFlower(Short.parseShort(vpChildNodes.item(5).getTextContent()));
			visualParameters.setAveLenFlowerMeasure(vpChildNodes.item(5).getAttributes().getNamedItem("measure").getTextContent());
		}
		return visualParameters;

	}

	private static GrowingParameters getGrowingParameters(Node item){
		GrowingParameters growingParameters = new GrowingParameters();
		NodeList gpChildNodes = item.getChildNodes();
		growingParameters.setTempreture(Short.parseShort(gpChildNodes.item(1).getTextContent()));
		growingParameters.setTemperatureMeasure(gpChildNodes.item(1).getAttributes().getNamedItem("measure").getTextContent());
		growingParameters.setLightRequiring(gpChildNodes.item(3).getAttributes().getNamedItem("lightRequiring").getTextContent());
		growingParameters.setWatering(Short.parseShort(gpChildNodes.item(5).getTextContent()));
		growingParameters.setWateringMeasure(gpChildNodes.item(5).getAttributes().getNamedItem("measure").getTextContent());

		return growingParameters;
	}

	public void sortXML(){
		flowers.getFlowers().sort(Comparator.comparing(Flower::getName));
	}

}
