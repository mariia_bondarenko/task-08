package com.epam.rd.java.basic.task8.entity;

/*
<tn:name>Роза</tn:name>
		<tn:soil>дерново-подзолистая</tn:soil>
		<tn:origin>Китай</tn:origin>
		<tn:visualParameters>
			<tn:stemColour>зеленая</tn:stemColour>
			<tn:leafColour>зеленая</tn:leafColour>
			<tn:aveLenFlower measure = "cm">10</tn:aveLenFlower>
		</tn:visualParameters>
		<tn:growingTips>
			<tn:tempreture measure = "celcius">25</tn:tempreture>
			<tn:lighting lightRequiring = "yes"/>
			<tn:watering measure = "mlPerWeek">110</tn:watering>
		</tn:growingTips>
		<tn:multiplying>черенки</tn:multiplying>
 */
public class Flower {
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingParameters growingParameters;
    private String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingParameters getGrowingParameters() {
        return growingParameters;
    }

    public void setGrowingParameters(GrowingParameters growingParameters) {
        this.growingParameters = growingParameters;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingParameters=" + growingParameters +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }


}
