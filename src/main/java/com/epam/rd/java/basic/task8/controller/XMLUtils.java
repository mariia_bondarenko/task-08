package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XMLUtils {

    public static void saveXML(String outputXML, Flowers flowers){
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("tn:flowers");
            root.setAttribute("xmlns:tn","http://com.epam.rd.java.basic.task8");
            root.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:schemaLocation", "http://com.epam.rd.java.basic.task8 input.xsd");
            document.appendChild(root);
            for (int i = 0; i < flowers.getFlowers().size(); i++) {
                Flower dataFlower = flowers.getFlowers().get(i);

                Element flower = document.createElement("flower");
                root.appendChild(flower);

                Element name = document.createElement("name");
                name.appendChild(document.createTextNode(dataFlower.getName()));
                flower.appendChild(name);

                Element soil = document.createElement("soil");
                soil.appendChild(document.createTextNode(dataFlower.getSoil()));
                flower.appendChild(soil);

                Element origin = document.createElement("origin");
                origin.appendChild(document.createTextNode(dataFlower.getOrigin()));
                flower.appendChild(origin);

                Element visualParameters = document.createElement("visualParameters");
                flower.appendChild(visualParameters);
                Element stemColour = document.createElement("stemColour");
                stemColour.appendChild(document.createTextNode(dataFlower.getVisualParameters().getStemColour()));
                visualParameters.appendChild(stemColour);
                Element leafColour = document.createElement("leafColour");
                leafColour.appendChild(document.createTextNode(dataFlower.getVisualParameters().getLeafColour()));
                visualParameters.appendChild(leafColour);
                Element aveLenFlower = document.createElement("aveLenFlower");
                aveLenFlower.appendChild(document.createTextNode(String.valueOf(dataFlower.getVisualParameters().getAveLenFlower())));
                aveLenFlower.setAttribute("measure",dataFlower.getVisualParameters().getAveLenFlowerMeasure());
                visualParameters.appendChild(aveLenFlower);

                Element growingParameters = document.createElement("growingTips");
                flower.appendChild(growingParameters);
                Element temperature = document.createElement("tempreture");
                temperature.appendChild(document.createTextNode(String.valueOf(dataFlower.getGrowingParameters().getTempreture())));
                temperature.setAttribute("measure",dataFlower.getGrowingParameters().getTemperatureMeasure());
                growingParameters.appendChild(temperature);
                Element lightRequiring = document.createElement("lighting");
                lightRequiring.setAttribute("lightRequiring",dataFlower.getGrowingParameters().getLightRequiring());
                growingParameters.appendChild(lightRequiring);
                Element watering = document.createElement("watering");
                watering.appendChild(document.createTextNode(String.valueOf(dataFlower.getGrowingParameters().getWatering())));
                watering.setAttribute("measure",dataFlower.getGrowingParameters().getWateringMeasure());
                growingParameters.appendChild(watering);

                Element multiplying = document.createElement("multiplying");
                multiplying.appendChild(document.createTextNode(dataFlower.getMultiplying()));
                flower.appendChild(multiplying);

            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(outputXML));
            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    public static void validateXML(String fileXMLName){
        Source xmlFile = new StreamSource(new File(fileXMLName));
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            File f = new File("input.xsd");
            Schema schema = schemaFactory.newSchema(f);
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            System.out.println(xmlFile.getSystemId() + " is valid");
        } catch (SAXException e) {
            System.out.println(xmlFile.getSystemId() + " is NOT valid reason:" + e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
